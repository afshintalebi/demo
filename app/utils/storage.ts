import * as localForage from 'localforage';

const store = localForage.createInstance({
    name: 'demo1234',
    storeName: 'demo1234-storage',
});

export interface StorageInterface {
    set: (key, value) => Promise<any>
    get: (key) => Promise<any | null>,
    remove: (key) => Promise<void>,
    length: (key) => Promise<number>,
}

export const Storage: StorageInterface = {
    set: async (key, value) => store.setItem(key, value),

    get: async key => store.getItem(key),

    remove: async key => store.removeItem(key),

    length: async () => store.length(),
};

