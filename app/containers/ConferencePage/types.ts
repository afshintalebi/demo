import { ActionType } from "typesafe-actions";
import * as actions from "./actions";
import {ContributorData} from "../../components/Contributors";

/* --- STATE --- */
interface ConferencePageState {
  items: ContributorData[];
}

/* --- ACTIONS --- */
type ConferencePageActions = ActionType<typeof actions>;

/* --- EXPORTS --- */
type ContainerState = ConferencePageState;
type ContainerActions = ConferencePageActions;

export { ContainerState, ContainerActions };
