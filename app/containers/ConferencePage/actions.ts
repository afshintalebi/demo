/*
 *
 * ConferencePage actions
 *
 */

import { action } from "typesafe-actions";
// import {ContainerState} from "./types";

import ActionTypes from "./constants";

export const addContributor = () => action(ActionTypes.ADD_CONTRIBUTER);

export const updateContributor = (data) => action(ActionTypes.UPDATE_CONTRIBUTER, data);

