/*
 * ConferencePage Messages
 *
 * This contains all the text for the ConferencePage container.
 */

import { defineMessages } from "react-intl";

export const scope = "app.containers.ConferencePage";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the ConferencePage container!",
  },
});
