/*
 *
 * ConferencePage
 *
 */

import React, {useEffect, useState} from "react";
import {Helmet} from "react-helmet-async";
// import {FormattedMessage} from "react-intl";
import {useSelector, useDispatch} from "react-redux";
import {createStructuredSelector} from "reselect";
import  Loader from "react-loader-spinner";
import {useInjectReducer, useInjectSaga} from "redux-injectors";
import makeSelectConferencePage, {makeSelectConferenceItems} from "./selectors";
import {addContributor, updateContributor} from "./actions";
import reducer from "./reducer";
import saga from "./saga";
// import messages from "./messages";
import Contributors from "../../components/Contributors";
import AddContributorButton from "../../components/AddContributorButton";
import styled from "styled-components";
import {Storage} from "../../utils/storage";
import {CONFERENCE_STORAGE_NAME} from "./constants";

const stateSelector = createStructuredSelector({
    conferencePage: makeSelectConferencePage(),
    items: makeSelectConferenceItems()
});

interface Props {
}

const Wrapper = styled.div`
    width: 100%;
    text-align: center;
`;

const initialData = [
    {
        name: "User 1",
        image: "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
    },
    {
        name: "User 2",
        image: "https://images.unsplash.com/photo-1552058544-f2b08422138a?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=944&q=80",
        isVertical: true
    },
    {
        name: "User 3",
        image: "https://images.unsplash.com/photo-1542103749-8ef59b94f47e?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=500&q=80"
    },
    {
        name: "User 4",
        image: "https://images.unsplash.com/photo-1504593811423-6dd665756598?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
    }
];



const StyledFooter = styled.div`
    position: fixed;
    bottom: 0;
    width: 100%;
    min-height: 50px;
    display: flex;
    align-items: center; 
    justify-content: center; 
    padding: 10px;
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function ConferencePage(props: Props) {
    useInjectReducer({key: "conferencePage", reducer: reducer});
    useInjectSaga({key: "conferencePage", saga: saga});
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const {items} = useSelector(stateSelector);
    const dispatch = useDispatch(); // eslint-disable-line @typescript-eslint/no-unused-vars

    useEffect(() => {
        Storage.get(CONFERENCE_STORAGE_NAME).then((data) => {
            const finalData = Array.isArray(data) && data.length > 0 ? data : initialData;
            dispatch(updateContributor(finalData));
        });
    }, [])

    const handleAdd = () => {
        if (items?.length < 12) {
            dispatch(addContributor());

        }
    }

    const renderLoader = ()=>(
        <Loader
            type="Bars"
            color="#00BFFF"
            height={40}
            width={40}
        />
    )

    const handleSortData = (sortedItems) => {
        dispatch(updateContributor(sortedItems))
    }


    return (
        <Wrapper>
            <Helmet>
                <title>Stand Up</title>
                <meta
                    name="description"
                    content="stand up"
                />
            </Helmet>
            {Array.isArray(items) && items.length  ? <Contributors items={items} onSortData={handleSortData}/> : renderLoader()}
            <StyledFooter>
                <AddContributorButton
                    onClick={handleAdd}
                    disabled={items?.length >= 12}
                />
            </StyledFooter>
        </Wrapper>
    );
}

export default ConferencePage;
