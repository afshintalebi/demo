/*
 *
 * ConferencePage constants
 *
 */

enum ActionTypes {
    ADD_CONTRIBUTER = "app/ConferencePage/ADD_CONTRIBUTER",
    UPDATE_CONTRIBUTER = "app/ConferencePage/UPDATE_CONTRIBUTER",
}

export default ActionTypes;

export const CONFERENCE_STORAGE_NAME = 'conference-contributors';
