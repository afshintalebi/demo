import { action } from 'typesafe-actions';

import { addContributor } from '../actions';
import ActionTypes from '../constants';

describe('ConferencePage actions', () => {
  describe('addContributor Action', () => {
    it('has a type of ADD_CONTRIBUTER', () => {
      const expected = action(ActionTypes.ADD_CONTRIBUTER);
      expect(addContributor()).toEqual(expected);
    });
  });
});
