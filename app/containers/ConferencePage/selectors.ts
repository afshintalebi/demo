import { createSelector } from "reselect";
import { ApplicationRootState } from "types";
import { ContainerState } from "./types";
import { initialState } from "./reducer";

/**
 * Direct selector to the conferencePage state domain
 */

const selectConferencePageDomain = (state: ApplicationRootState) =>
  state.conferencePage || initialState;

/**
 * Other specific selectors
 */

/**
 * Default selector used by ConferencePage
 */

const makeSelectConferencePage = () =>
  createSelector(selectConferencePageDomain, (substate) => substate);

const makeSelectConferenceItems = () =>
  createSelector(selectConferencePageDomain, (substate) => substate.items);

export default makeSelectConferencePage;
export { selectConferencePageDomain, makeSelectConferenceItems };
