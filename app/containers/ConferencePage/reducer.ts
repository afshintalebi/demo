/*
 *
 * ConferencePage reducer
 *
 */

import ActionTypes, {CONFERENCE_STORAGE_NAME} from "./constants";
import {ContainerState, ContainerActions} from "./types";
import {Storage} from "../../utils/storage";

function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}

const additionalData = [
    {
        name: "User ",
        image: "https://images.unsplash.com/photo-1547425260-76bcadfb4f2c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
    },
    {
        name: "User ",
        image: "https://images.unsplash.com/photo-1595399874399-10f2444c4eb2?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80",
        isVertical: true
    },
    {
        name: "User ",
        image: "https://images.unsplash.com/photo-1500048993953-d23a436266cf?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
    },
];

export const initialState: ContainerState = {
    items: [],
};

function conferencePageReducer(
    state: ContainerState = initialState,
    action: ContainerActions
): ContainerState {
    switch (action.type) {
        case ActionTypes.ADD_CONTRIBUTER:
            const newData = Object.assign({}, additionalData[Math.ceil(getRandomArbitrary(0, 2))]);
            newData.name += (state.items.length + 1).toString();
            const newState = {
                ...state,
                ...{items: [...state.items, ...[newData]]}
            };
            // save new data in storage
            Storage.set(CONFERENCE_STORAGE_NAME, newState.items).then(() => console.log('storage saved'));
            return newState;
        case ActionTypes.UPDATE_CONTRIBUTER:
            const newItems = action.payload;
            // save new data in storage
            Storage.set(CONFERENCE_STORAGE_NAME, newItems).then(() => console.log('storage updated'));

            return {
                ...state,
                ...{items: newItems}
            };
        default:
            return state;
    }
}

export default conferencePageReducer;
