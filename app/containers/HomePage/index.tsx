/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import * as React from 'react';
// import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';
// import messages from './messages';
import Contributors from "../../components/Contributors";
import AddContributorButton from "../../components/AddContributorButton";

const Wrapper = styled.div`

`

const StyledFooter = styled.div`
    position: fixed;
    bottom: 0;
    width: 100%;
    min-height: 50px;
    display: flex;
    align-items: center; 
    justify-content: center; 
    padding: 10px;
`

export default function HomePage() {
    const items = [
        {
            name: "User 1",
            image: "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
        },
        {
            name: "User 2",
            image: "https://images.unsplash.com/photo-1552058544-f2b08422138a?ixlib=rb-1.2.1&ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&auto=format&fit=crop&w=944&q=80",
            isVertical: true
        },
        {
            name: "User 3",
            image: "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
        },
        {
            name: "User 3",
            image: "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=80"
        }
    ];

    return (
        <Wrapper>
            <Contributors items={items}/>
            <StyledFooter>
            </StyledFooter>
        </Wrapper>
    );
}
