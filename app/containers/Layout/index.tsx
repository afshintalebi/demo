/*
 *
 * Layout
 *
 */

import React from "react";
import styled from "styled-components";
import { FormattedMessage } from "react-intl";
import { useSelector, useDispatch } from "react-redux";
import { createStructuredSelector } from "reselect";

import { useInjectReducer, useInjectSaga } from "redux-injectors";

import makeSelectLayout from "./selectors";
import reducer from "./reducer";
import saga from "./saga";
import messages from "./messages";

const stateSelector = createStructuredSelector({
  layout: makeSelectLayout(),
});

interface Props {
  children: React.ReactNode
}

const Wrapper = styled.div`
   margin-top: -26px;
   height: 104vh;
   background: rgb(13,15,16);
   background: linear-gradient(180deg, rgba(13,15,16,1) 0%, rgba(32,35,39,0.9626225490196079) 13%, rgba(49,54,61,1) 25%, rgba(49,54,61,1) 85%, rgba(30,33,37,0.9346113445378151) 93%, rgba(13,15,16,1) 100%);
   display: flex;
   justify-content: center;
   align-items: center;
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function Layout({children}: Props) {
  useInjectReducer({ key: "layout", reducer: reducer });
  useInjectSaga({ key: "layout", saga: saga });

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { layout } = useSelector(stateSelector);
  const dispatch = useDispatch(); // eslint-disable-line @typescript-eslint/no-unused-vars
  return (
    <Wrapper>
      {children}
    </Wrapper>
  );
}

export default Layout;
