/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import * as React from 'react';
import {Switch, Route} from 'react-router-dom';
import {hot} from 'react-hot-loader/root';

import ConferencePage from 'containers/ConferencePage';
import NotFoundPage from 'containers/NotFoundPage/Loadable';

import GlobalStyle from '../../global-styles';
import Layout from "../Layout";

function App() {
    return (
        <div>
            <Layout>
                <Switch>
                    <Route exact path="/" component={ConferencePage}/>
                    <Route component={NotFoundPage}/>
                </Switch>
            </Layout>
            <GlobalStyle/>
        </div>
    );
}

export default hot(App);
