/*
 * ContributeCol Messages
 *
 * This contains all the text for the ContributeCol component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.ContributeCol";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the ContributeCol component!",
  },
});
