/**
 *
 * ContributeCol
 *
 */
import React from "react";

import styled from 'styles/styled-components';

// import {FormattedMessage} from "react-intl";
// import messages from "./messages";
import ContributorImage, {ImageSize} from "../ContributorImage";

interface Props {
    width: string,
    name: string,
    size: ImageSize | 'medium',
    url: string,
    imageVertical?: boolean,
}

const Wrapper: any = styled.div`
    width: ${(props: any) => props.width};
    height: auto;
    max-width: ${(props: any) => props.width};
    max-height: 100%;
    text-align: center;
    position: relative;
    overflow: hidden;
`;

const StyledName: any = styled.div`
    position: absolute;
    bottom: 5px;
    left:0;
    right:0;
    margin-left:auto;
    margin-right:auto;
    width: auto;
    max-width: 100px;
    background: rgba(0,0,0,0.7);
    color: white;
    text-align: center;
    border-radius: 50px;
    padding: 2px;
`;


// eslint-disable-next-line @typescript-eslint/no-unused-vars
function ContributeCol({width, name, size, url, imageVertical=false}: Props) {
    return (
        <Wrapper width={width}>
            <ContributorImage
                size={size}
                imageUrl={url}
                isVertical={imageVertical}
            />
            <StyledName>{name}</StyledName>
        </Wrapper>
    );
}

export default ContributeCol;
