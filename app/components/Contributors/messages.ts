/*
 * Contributers Messages
 *
 * This contains all the text for the Contributers component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.Contributers";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the Contributers component!",
  },
});
