/**
 *
 * Contributers
 *
 */
import React from "react";
import {SortableContainer, SortableElement} from "react-sortable-hoc";
import arrayMove from 'array-move';
import styled from 'styled-components';
// import {FormattedMessage} from "react-intl";
import ContributeCol from "../ContributeCol";
// import messages from "./messages";

export interface ContributorData {
    name: string;
    image: string;
    isVertical?: boolean;
}

interface Props {
    items: ContributorData[];
    onSortData?: (sortedItems: any[]) => void;
}

const getColSize = (count) => {
    let size: any = 'large';
    if (count > 8)
        size = 'small';
    else if (count > 4)
        size = 'medium';

    return size;
}

const getColWidth = (count) => {
    let width = '45%';
    if (count > 4) {
        width = '21%;'
    }
    return width;
}

const Wrapper: any = styled.div`
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    width: 70%;
    margin: 0 auto;
    > div {
        margin: 5px;
        flex: ${(props: any) => `1 0 ${getColWidth(props.count)}`};
    }
`;

const SortableItem: any = SortableElement(({width, size, url, name, isVertical}) => (
    <ContributeCol
        name={name}
        width={width}
        size={size}
        imageVertical={!!isVertical}
        url={url}
    />
));

const SortableList = SortableContainer(({items}) => {
    const width = getColWidth(items.length);
    const size = getColSize(items.length);

    return (
        <Wrapper count={items.length}>
            {items.map(({image, name, isVertical}: ContributorData, index) => (
                <SortableItem
                    key={`item-${index}`}
                    index={index}
                    name={name}
                    width={width}
                    size={size}
                    imageVertical={!!isVertical}
                    url={image}/>
            ))}
        </Wrapper>
    );
});


// eslint-disable-next-line @typescript-eslint/no-unused-vars
function Contributors({items, onSortData}: Props) {
    const onSortEnd = ({oldIndex, newIndex}) => {
        const newItems = arrayMove(items, oldIndex, newIndex);
        if(onSortData)
            onSortData(newItems);
        /*this.setState(({items}) => ({
            items: arrayMove(items, oldIndex, newIndex),
        }));*/
    };
    return <SortableList items={items} onSortEnd={onSortEnd} axis="xy"/>;
    /*const width = getColWidth(items.length);
    const size = getColSize(items.length);
    return (
        <Wrapper count={items.length}>
            {items.map(({image, name, isVertical}: ContributorData, index) => (
                <ContributeCol
                    key={index}
                    name={name}
                    width={width}
                    size={size}
                    imageVertical={!!isVertical}
                    url={image}
                />
            ))}
        </Wrapper>
    );*/
}

export default Contributors;
