/*
 * AddContributorButton Messages
 *
 * This contains all the text for the AddContributorButton component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.AddContributorButton";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the AddContributorButton component!",
  },
});
