/**
 *
 * AddContributorButton
 *
 */
import React from "react";

import styled from 'styles/styled-components';

import {FormattedMessage} from "react-intl";
import messages from "./messages";

interface Props {
    [key: string]: any,
}

const StyledAddButton = styled.button`
     background: #09c134;
     cursor: pointer;
     color: white;
     border: none;
     font-size: 30px;
     border-radius: 50%;
     width: 40px;
     height: 40px;
     &:active, &:focus {
        border: none;
        outline: none;
     }
`

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function AddContributorButton({...props}: Props) {
    return (
        <StyledAddButton type="button" {...props}>+</StyledAddButton>
    );
}

export default AddContributorButton;
