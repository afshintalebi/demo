/**
 *
 * ContributerImage
 *
 */
import React from "react";

import styled from 'styles/styled-components';

import {FormattedMessage} from "react-intl";
import messages from "./messages";

declare const ImageSizes: ["large", "medium", "small", "tiny"];
export declare type ImageSize = typeof ImageSizes[number];

interface Props {
    imageUrl?: string;
    isVertical?: boolean;
    size?: ImageSize | "medium";
}

const height = {
    "large": 350,
    "medium": 280,
    "small": 180,
    "tiny": 80,
}

const StyledImage: any = styled.img`
    width: auto;
    max-height: 100%;
    max-width: ${(props: any) => props.isVertical ? '300px' : '100%'};
    height: ${(props: any) => height[props.size]}px;
    object-fit: cover;
`;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function ContributorImage({imageUrl, size, isVertical = false}: Props) {
    return (
        <StyledImage
            src={imageUrl}
            size={size}
            isVertical={isVertical}
        />
    );
}

export default ContributorImage;
