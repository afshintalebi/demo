/*
 * ContributerImage Messages
 *
 * This contains all the text for the ContributerImage component.
 */

import { defineMessages } from "react-intl";

export const scope = "app.components.ContributerImage";

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: "This is the ContributerImage component!",
  },
});
