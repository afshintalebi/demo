## Demo  Project
This is a sample code

## Install & View
- Type `npm i` to install dependencies
- To view web app in `development`, type `npm start`
- To view web app in `production`, run `npm run build && npm run start:prod`

## Project Structure
- `app`: Source of project contains `containerss`, `components`, `utilities`, `styles`, `etc`
- `build`: Build version of project
- `docs`: Project document
- `internals`: Webpack & boilerplate configs
- `server`: server source

## Document
[View Document](docs/README.md)

## Third-Party Libraries
- [react-sortable-hoc](https://github.com/clauderic/react-sortable-hoc)
- [localforage](https://github.com/localForage/localForage)
